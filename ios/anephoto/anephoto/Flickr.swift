//
//  Flickr.swift
//  anephoto
//
//  Created by Jose Antonio Cano Gómez on 9/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import Foundation
import UIKit

class Flickr {
    let apiKey: String = "311ea6dee5caafceeafc171d3f68f110"
    var user: String = "127591027@N02" //"131124626@N07"
    
    init (){
    }
    
    func getGalleries (result: (galleries: [Gallery], error: NSError?) -> Void) {
        let url = "https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=\(self.apiKey)&user_id=\(self.user)&format=json&nojsoncallback=1"
        
        getURL(url) { (object, error) -> Void in
            //println(object)
            var galleries: [Gallery] = []
            
            let galleriesContainer = object!["photosets"] as NSDictionary
            let galleriesResults = galleriesContainer["photoset"] as NSArray
            
            for gallery in galleriesResults
            {
                let id = gallery["id"] as? String ?? ""
                let title = (gallery["title"] as NSDictionary)["_content"] as? String ?? ""
                galleries.append(Gallery(id: id, title: title))
            }
            
            result(galleries: galleries, error: nil)
        }
    }
    
    func getPhotos (gallery: String, result: (pictures: [Photo], error: NSError?) -> Void){
        let url = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=\(self.apiKey)&photoset_id=\(gallery)&format=json&nojsoncallback=1"
        
        getURL(url) { (object, error) -> Void in
            //println(object)
            var pictures: [Photo] = []
            
            let photosContainer = object!["photoset"] as NSDictionary
            let photosItems = photosContainer["photo"] as NSArray
            
            for photo in photosItems
            {
                let id = photo["id"] as? String ?? ""
                let title = photo["title"] as? String ?? ""
                let farm = photo["farm"] as? Int ?? 0
                let server = photo["server"] as? String ?? ""
                let secret = photo["secret"] as? String ?? ""
                
                pictures.append(Photo(id: id, title: title, farm: farm, server: server, secret: secret))
            }
            
            result(pictures: pictures, error: nil)
        }

    }
    
    func getFirstPhoto (gallery: String, result: (pictures: [Photo], error: NSError?) -> Void){
        let url = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=\(self.apiKey)&photoset_id=\(gallery)&format=json&nojsoncallback=1"
        
        getURL(url) { (object, error) -> Void in
            //println(object)
            var pictures: [Photo] = []
            
            let photosContainer = object!["photoset"] as NSDictionary
            let photosItems = photosContainer["photo"] as NSArray
            
            for photo in photosItems
            {
                let id = photo["id"] as? String ?? ""
                let title = photo["title"] as? String ?? ""
                let farm = photo["farm"] as? Int ?? 0
                let server = photo["server"] as? String ?? ""
                let secret = photo["secret"] as? String ?? ""
                
                pictures.append(Photo(id: id, title: title, farm: farm, server: server, secret: secret))
                break
            }
            
            result(pictures: pictures, error: nil)
        }
        
    }
    
    func getPhotoInfo(photo: Photo, result: (error: NSError?) -> Void){
        let url = "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=\(self.apiKey)&photo_id=\(photo.id)&format=json&nojsoncallback=1"
            
        getURL(url) { (object, error) -> Void in
            println(object)
            let photoContainer = object!["photo"] as NSDictionary
            let description = (photoContainer["description"] as NSDictionary)["_content"] as? String ?? ""
            
            photo.description = description
            
            result(error: nil)
        }
    }

    
    func getPersonalInfo (result: (person: Person, error: NSError?) -> Void ) {
        let url = "https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=\(self.apiKey)&user_id=\(self.user)&format=json&nojsoncallback=1"
        
        getURL(url) { (object, error) -> Void in
            println(object)
            
            let personContainer = object!["person"] as NSDictionary
            let description = (personContainer["description"] as NSDictionary)["_content"] as? String ?? ""
            let name = (personContainer["realname"] as NSDictionary)["_content"] as? String ?? ""
            
            result(person: Person(name: name, description: description), error: nil)
        }        
    }
    
    private func getURL(call:String, result: (object: AnyObject?, error: NSError?) -> Void)  {
        var url = NSURL(string: call)!
        let loadRequest = NSURLRequest(URL:url)
        NSURLConnection.sendAsynchronousRequest(loadRequest,
            queue: NSOperationQueue.mainQueue()) {
                response, data, error in
                
                if error != nil {
                    result(object: nil, error: error)
                    return
                }
                
                if data != nil {
                    var JSONError : NSError?
                    let resultsDictionary = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions(0), error: &JSONError) as? NSDictionary
                    
                    
                    result(object: resultsDictionary!, error: nil)
                    return
                }
                
                result(object: nil, error: nil)
        }
        
    
    }
    
}