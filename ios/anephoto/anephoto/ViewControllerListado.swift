//
//  ViewControllerListado.swift
//  anephoto
//
//  Created by Kike Beltrán on 8/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

let reuseIdentifier = "CellCollection"

class ViewControllerListado: UICollectionViewController {

    var pictures: [Photo] = []
    var gallery: Gallery?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.navigationItem.title = "ANE LAGERQVIST"
        
        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        dispatch_async(dispatch_get_main_queue(), {
            var appFlickr = Flickr()
            appFlickr.getPhotos(self.gallery!.id) { (object, error) -> Void in
                self.pictures = object as [Photo]
                self.collectionView!.reloadData()
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "DetailPicturesToDetail" {
            var navigationController = segue.destinationViewController as UINavigationController
            var detalleViewController = navigationController.viewControllers[0] as ViewControllerDetalle
            
            var celda = sender as NSIndexPath
            var photo = pictures[celda.row]
            detalleViewController.photo = photo
            detalleViewController.title = photo.title
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return self.pictures.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as UICollectionViewCell
    
        
        var p = self.pictures[indexPath.row]
        if (p.thumbnail == nil)
        {
            
            dispatch_async(dispatch_get_main_queue(), {
                
                p.thumbnail(size: "m", result: { (picture, error) -> Void in
                    p.thumbnail = picture
                    cell.backgroundView = UIImageView(image: p.thumbnail)
                    
                })
            })
        }
        else
        {
            cell.backgroundView = UIImageView(image: p.thumbnail)
        }
        
        
        
        return cell
    }

    
    
    
    // MARK: UICollectionViewDelegate

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("DetailPicturesToDetail", sender: indexPath)
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
