//
//  ViewControllerContacto.swift
//  anephoto
//
//  Created by Joaquin Gil Silva on 18/3/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

import MessageUI


class ViewControllerContacto: UIViewController, MFMailComposeViewControllerDelegate {
    
    var alert: UIAlertView?
    var subjectText:String?
    var destinatario:AnyObject!
    var mailController:MFMailComposeViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Recipients
        subjectText = "Contacto"
        destinatario = "jgs806@hotmail.com"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func enviarEmail(sender: AnyObject) {
    
        if(MFMailComposeViewController.canSendMail()){
            
            //alertview
            alert = UIAlertView()
            alert!.addButtonWithTitle("Ok")
            
            mailController = MFMailComposeViewController()
            //asignar delegado al controlador de email
            mailController?.mailComposeDelegate = self
            
            //Completar objeto mailController
            mailController?.setSubject(subjectText)
            
            var recipients = [destinatario]
            
            mailController?.setToRecipients(recipients)
            self.presentViewController(mailController!, animated: true, completion: nil)
        }else
        {
            let sendMailErrorAlert = UIAlertView (title: "No se puede enviar Email", message: "Por favor configura tu App Mail para poder enviar correos", delegate: self, cancelButtonTitle: "Entendido")
            sendMailErrorAlert.show()
            self.dismissViewControllerAnimated(false, completion: nil)
        }
        
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        switch result.value {
            
        case MFMailComposeResultCancelled.value:
            //se cancelo envio
            alert!.title = "Envio cancelado"
            alert!.message = "Se cancelo el envio"
            alert!.show()
            
        case MFMailComposeResultSaved.value:
            //se guardo
            alert!.title = "Correo guardado"
            alert!.message = "Se guardo el correo en Mail"
            alert!.show()
            
        case MFMailComposeResultFailed.value:
            //fallo el envio
            alert!.title = "Error"
            alert!.message = "El correo no pudo ser enviado"
            alert!.show()
            
        case MFMailComposeResultSent.value:
            //se cancelo envio
            alert!.title = "Correo enviado"
            alert!.message = "El correo se envio correctamente"
            alert!.show()
            
        default:
            break
            
            
        }
        
        mailController?.dismissViewControllerAnimated(true,
            
            //closure a ejecutar al finalizar de mostrar la vista
            
            { () -> Void in
                
                // cerrar pantalla del view base
                self.dismissViewControllerAnimated(true, completion: nil)
        })
    }

    @IBAction func twiter(sender: AnyObject) {
    }
    
    @IBAction func instagram(sender: AnyObject) {
    }
}

