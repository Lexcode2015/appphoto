//
//  Person.swift
//  anephoto
//
//  Created by Jose Antonio Cano Gómez on 11/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import Foundation

class Person {
    var name: String
    var description: String
    
    init(name: String, description: String){
        self.name = name
        self.description = description
    }
}