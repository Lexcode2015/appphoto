//
//  ViewControllerCategories.swift
//  anephoto
//
//  Created by Kike Beltrán on 8/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

class ViewControllerCategories: UIViewController , UITableViewDelegate, UITableViewDataSource {

    var galleries : [Gallery] = []
    @IBOutlet weak var tableGalleries: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ANE LAGERQVIST"
        
        tableGalleries.delegate = self
        
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(dispatch_get_main_queue(), {
            var appFlickr = Flickr()
            appFlickr.getGalleries { (object, error) -> Void in
                self.galleries = object as [Gallery]
                self.tableGalleries.reloadData()
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Table view data source
    


    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.galleries.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellGallery", forIndexPath: indexPath) as CustomTableViewCell
        var gallery = galleries[indexPath.row]
        
        Flickr().getFirstPhoto(gallery.id, result: { (pictures, error) -> Void in
            var photo: Photo = pictures[0]
            cell.GalleryLabel.text = gallery.title
            
            if (gallery.image == nil)
            {
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        println(indexPath.row)
                        
                        photo.thumbnail(size: "s", result: { (picture, error) -> Void in
                            gallery.image = picture
                            cell.GalleryImage.image = gallery.image
                            cell.tag = indexPath.row
                        })
                        
                        
                    })
                //}
                
            }
            else
            {
                cell.GalleryImage.image = gallery.image
                cell.tag = indexPath.row
            }           
            
        })
        
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "GalleryToDetailPictures" {
            var navigationController = segue.destinationViewController as UINavigationController
            var detalleViewController = navigationController.viewControllers[0] as ViewControllerListado
            var celda = sender as UITableViewCell
            var gallery = galleries[celda.tag]
            detalleViewController.gallery = gallery
            detalleViewController.title = gallery.title
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
