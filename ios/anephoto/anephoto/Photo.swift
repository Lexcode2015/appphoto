//
//  Photo.swift
//  anephoto
//
//  Created by Jose Antonio Cano Gómez on 9/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import Foundation
import UIKit

class Photo {
    var largeImage : UIImage?
    var thumbnail: UIImage?
    var id : String = ""
    var title : String = ""
    var farm : Int = 0
    var server : String = ""
    var secret : String = ""
    
    var description: String = ""
    
    init (id: String) {
        self.id = id
    }
    
    init (id:String, title: String, farm:Int, server:String, secret:String) {
        self.id = id
        self.title = title
        self.farm = farm
        self.server = server
        self.secret = secret
    }
    
    /*
    s: width="75" height="75"
    q: width="150" height="150"
    t: width="100" height="75"
    m: width="240" height="180"
    n: width="320" height="240"
    []: width="500" height="375"
    z ?zz=1: width="640" height="480"
    c: width="800" height="600"
    b: width="1024" height="768"
    o: width="2400" height="1800"
    */
    
    func flickrImageURL(size:String = "m") -> NSURL {
        return NSURL(string: "http://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_\(size).jpg")!
    }

    func thumbnail(size: String = "m", result: (picture: UIImage?, error: NSError? ) -> Void)
    {
     /*   let imageData: NSData? = NSData(contentsOfURL: )
        var image = UIImage(data: imageData!)
        return image!*/
        let loadRequest = NSURLRequest(URL:self.flickrImageURL(size: size))
        NSURLConnection.sendAsynchronousRequest(loadRequest,
            queue: NSOperationQueue.mainQueue()) {
                response, data, error in
                
                if error != nil {
                    result(picture: nil, error: error)
                    return
                }
                
                if data != nil {
                    let returnedImage = UIImage(data: data)
                    //self.largeImage = returnedImage
                    result(picture: returnedImage!, error: nil)
                    return
                }
                
                result(picture: nil, error: nil)
        }

    }
    
}