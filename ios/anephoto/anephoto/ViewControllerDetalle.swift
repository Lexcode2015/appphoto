//
//  ViewControllerDetalle.swift
//  anephoto
//
//  Created by Kike Beltrán on 8/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

class ViewControllerDetalle: UIViewController {

    var photo: Photo?
    
    @IBOutlet weak var photoDetail: UIImageView!
    
    @IBOutlet weak var TextDetail: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var appFlickr = Flickr()
        appFlickr.getPhotoInfo(self.photo!) { (error) -> Void in
            
            self.photoDetail.backgroundColor = UIColor.redColor()

            self.photo?.thumbnail(size: "m", result: { (picture, error) -> Void in
                self.photoDetail.image = picture
            })
            
            self.TextDetail.text = self.photo?.description
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
