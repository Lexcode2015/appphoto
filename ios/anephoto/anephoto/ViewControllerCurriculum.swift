//
//  ViewControllerCurriculum.swift
//  anephoto
//
//  Created by Kike Beltrán on 8/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

class ViewControllerCurriculum: UIViewController {

    @IBOutlet weak var CV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.      
        var appFlickr = Flickr()
        appFlickr.getPersonalInfo() { (person, error) -> Void in
            self.title = person.name
            self.CV.text = person.description
        }
        
        
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Ocultamos la barra de estado
    //override func prefersStatusBarHidden() -> Bool {
        //return true;
    //}

}
