//
//  CustomTableViewCell.swift
//  anephoto
//
//  Created by Jose Antonio Cano Gómez on 16/2/15.
//  Copyright (c) 2015 Kike Beltrán. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {


    @IBOutlet weak var GalleryLabel: UILabel!
    @IBOutlet weak var GalleryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
